import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductAddForm.module.css';

function ProductAddForm({ onProductAdd }) {
	const [product, setProduct] = React.useState({
		imageUrl: '',
		name: '',
		price: '',
	});

	const handleChange = (event) => {
		event.persist();
		setProduct((prev) => ({
			...prev,
			[event.target.id]: event.target.value,
		}));
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		onProductAdd(product);
		setProduct({
			imageUrl: '',
			name: '',
			price: 0,
		});
	};

	return (
		<div className={styles['product-form']}>
			<form onSubmit={handleSubmit}>
				<h3>Add new product</h3>
				<label htmlFor='imageUrl'>Image url</label>
				<input
					id='imageUrl'
					type='text'
					required
					className='form-input'
					value={product.imageUrl}
					onChange={handleChange}
				/>
				<label htmlFor='name'>Product name</label>
				<input
					id='name'
					type='text'
					required
					className='form-input'
					value={product.name}
					onChange={handleChange}
				/>
				<label htmlFor='price'>Price</label>
				<input
					id='price'
					type='number'
					step='0.01'
					required
					className='form-input'
					value={product.price}
					onChange={handleChange}
				/>
				<button type='submit' className='btn btn--primary'>
					Add
				</button>
			</form>
		</div>
	);
}

ProductAddForm.propTypes = {
	onProductAdd: PropTypes.func.isRequired,
};

export default ProductAddForm;
