import React from 'react';
import PropTypes from 'prop-types';
import styles from './Cart.module.css';

function Cart({ count }) {
	return <div className={styles.cart}>Cart({count})</div>;
}

Cart.propTypes = {
	count: PropTypes.number.isRequired,
};

export default Cart;
