import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductCard.module.css';

function ProductCard({ product, onProductRemove, onAddToCart }) {
	return (
		<div className={styles['product-card']}>
			<div className={styles['product-card__img']}>
				<img src={product.imageUrl} alt='product-img' />
			</div>
			<h3 className={styles['product-card__price']}>
				{Number(product.price).toFixed(2)}€
			</h3>
			<p className={styles['product-card__name']}>{product.name}</p>
			<div className={styles['product-card-actions']}>
				<button className='btn btn--primary' onClick={() => onAddToCart()}>
					Add to cart
				</button>
				<button className='btn btn--outline' onClick={onProductRemove}>
					Remove
				</button>
			</div>
		</div>
	);
}

ProductCard.propTypes = {
	product: PropTypes.shape({
		imageUrl: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	}).isRequired,
	onProductRemove: PropTypes.func.isRequired,
	onAddToCart: PropTypes.func.isRequired,
};

export default ProductCard;
