import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductGrid.module.css';
import ProductCard from '../ProductCard/ProductCard';

function ProductGrid({ products, onProductRemove, onAddToCart }) {
	return (
		<div className={styles['product-grid']}>
			{products?.map((product, idx) => (
				<div key={product.name + idx} className={styles['product-grid__col']}>
					<ProductCard
						product={product}
						onProductRemove={() => onProductRemove(idx)}
						onAddToCart={onAddToCart}
					/>
				</div>
			))}
		</div>
	);
}

ProductGrid.propTypes = {
	products: PropTypes.arrayOf(
		PropTypes.shape({
			imageUrl: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
				.isRequired,
		})
	).isRequired,
	onProductRemove: PropTypes.func.isRequired,
	onAddToCart: PropTypes.func.isRequired,
};

export default ProductGrid;
