import React from 'react';
import products from '../../products.json';
import styles from './Products.module.css';
import Cart from '../../components/Cart/Cart';
import ProductGrid from '../../components/ProductGrid/ProductGrid';
import ProductAddForm from '../../components/ProductAddForm/ProductAddForm';

function Products() {
	const [productList, setProductList] = React.useState(products);
	const [cartCount, setCartCount] = React.useState(0);

	const removeProduct = (productIndex) => {
		setProductList((prev) => {
			const newValue = [...prev];
			newValue.splice(productIndex, 1);
			return newValue;
		});
	};

	const addProductToCart = () => setCartCount((prev) => prev + 1);

	const addNewProductToList = (product) =>
		setProductList([product, ...productList]);

	return (
		<div>
			<div className={styles['page-container']}>
				<div className={styles['page-container__form']}>
					<ProductAddForm onProductAdd={addNewProductToList} />
				</div>
				<div className={styles['page-container__cart']}>
					<Cart count={cartCount} />
				</div>
			</div>
			<ProductGrid
				products={productList}
				onProductRemove={removeProduct}
				onAddToCart={addProductToCart}
			/>
		</div>
	);
}

export default Products;
